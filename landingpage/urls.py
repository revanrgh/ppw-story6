from django.urls import include, path
from .views import index, post

urlpatterns = [
	path('index', index, name="index"),
	path('post', post, name="post"),
	path('', index),
]

