from django.urls import include, path
from .views import registration, create_user, validate_email, displaySubs, deleteUser

urlpatterns = [
	path('', registration, name="regis"),
	path('create-user/', create_user),
	path('validate/', validate_email),
	path('display/', displaySubs),
	path('delete/', deleteUser),
]

