from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Status
from .forms import StatusForm
from .views import index, post

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story6UnitTest(TestCase):
	def test_story_6_url_is_exist(self):
		response = Client().get('/landingpage/')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_index_template(self):
		response = Client().get('/landingpage/')
		self.assertTemplateUsed(response, 'index.html')

	def test_story_6_using_index_func(self):
		found = resolve('/landingpage/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		#Creating a new activity
		new_status = Status.objects.create(status='Test Status')

		#Retrieving all available activity
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)
		self.assertEqual(new_status.__str__(), new_status.status)
 
	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'],["This field is required."])

	def test_story_6_post_success_and_render_the_result(self):
		test = 'Test status'
		response_post = Client().post('/landingpage/post', {'status' : test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/landingpage/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_story_6_post_error_and_render_the_result(self):
		test = 'Test status'
		response_post = Client().post('/landingpage/post', {'status': ''})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/landingpage/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

# class Story7FunctionalTest(TestCase):
# 	def setUp(self):
# 		chrome_options = webdriver.chrome.options.Options()
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
# 		self.selenium.implicitly_wait(25)
# 		super(Story7FunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story7FunctionalTest, self).tearDown()

# 	def test_input_status_form(self):
# 		selenium = self.selenium
# 		# Opening the link
# 		selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		# Find the form element
# 		status = selenium.find_element_by_id('id_status')
# 		submit = selenium.find_element_by_name('submit')

# 		# Fill the form with data
# 		status.send_keys('Coba-coba')
# 		time.sleep(3)

# 		# submitting the form
# 		submit.send_keys(Keys.RETURN)

# 		# check the returned result
# 		assert 'Coba-coba' in selenium.page_source

# 	# STORY 7

# 	# Check the page title
# 	def test_page_title(self):
# 		self.selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		self.assertIn('STORY 6 - Landing Page', self.selenium.title)

# 	# Check the text in h1 and h2
# 	def test_heading_text_in_page(self):
# 		self.selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		heading1 = self.selenium.find_element_by_tag_name('h1').text
# 		heading2 = self.selenium.find_element_by_tag_name('h2').text
# 		self.assertIn('Hello,', heading1)
# 		self.assertIn('Apa kabar?', heading2)

# 	# Check the body background-color == #A51313
# 	def test_css_background(self):
# 		self.selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		element_color = "rgba(165, 19, 19, 1)"
# 		body_element = self.selenium.find_element_by_css_selector('body')
# 		body_color = body_element.value_of_css_property('background-color')
# 		self.assertEqual(body_color, element_color)

# 	# Check the button-text-color == white
# 	def test_button_text_color(self):
# 		self.selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		element_color = "rgba(255, 255, 255, 1)"
# 		btn_text_element = self.selenium.find_element_by_class_name('btn')
# 		btn_text_color = btn_text_element.value_of_css_property('color')
# 		self.assertEqual(btn_text_color, element_color)
