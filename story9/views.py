from django.shortcuts import render
import json
from urllib.request import urlopen
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.contrib.auth import logout

# Create your views here.
response = {}
def index_daftarbuku(request):
	context = {}
	if request.session.get('totalBooks') != None and request.session.get('_auth_user_id_') != None:
		context['adaBuku'] = request.session['totalBooks']
		print("Masuk sini")
	return render(request, 'index-daftarbuku.html', context)

	
def get_booklists(request):
	with urlopen("https://www.googleapis.com/books/v1/volumes?q=quilting") as response:
		source = response.read()

	data = json.loads(source)
	return JsonResponse(data)
	# print(json.dumps(data, indent=2))

	# for item in data['items']:
	# 	title = item['volumeInfo']['title']
	# 	# console.log("Books title: " + title)
	# 	list_data.append(title)

	# response['book_list_title'] = list_data
	# return render(request, html, response)

def api_search_book(request, search):
	with urlopen("https://www.googleapis.com/books/v1/volumes?q=" + search) as response:
		source = response.read()

	print("CARI DATA")
	dataCari = json.loads(source)
	return JsonResponse(dataCari)

def login(request):
	print("login page")
	return render(request, 'login.html')

def logout_view(request):
	logout(request)
	print("logout")
	return HttpResponseRedirect('/daftar-buku')

def updateBooks(request):
	request.session['totalBooks'] = request.GET['counter']
	print("Jumlah buku: " + request.session['totalBooks'])
	return HttpResponse("Data masuk")

# def auth_login(request):
#     print ("----auth_login----")

#     if request.method == "POST":
#         username = request.POST['username']
#         password = request.POST['password']

