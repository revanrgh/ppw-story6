from django.test import TestCase, Client
from django.urls import resolve
from .views import registration, create_user
from .models import User

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story10UnitTest(TestCase):
	def test_story_10_url_is_exist(self):
		response = Client().get('/registrasi/')
		self.assertEqual(response.status_code, 200)

	def test_story_10_using_index_template(self):
		response = Client().get('/registrasi/')
		self.assertTemplateUsed(response, 'index-registrasi.html')

	def test_story_10_using_index_func(self):
		found = resolve('/registrasi/')
		self.assertEqual(found.func, registration)

	def test_model_can_create_new_user(self):
		new_user = User.objects.create(nama='Saya', email='emailku@gmail.com', password='inipassword')
		counting_all_user = User.objects.all().count()
		# response = Client().post('/registrasi/create-user')
		# html_response = response.content.decode('utf8')
		self.assertEqual(counting_all_user, 1)
		# self.assertIn("OK", html_response)

	# def test_create_user_func(self):
	# 	found = resolve('/create-user/')
	# 	self.assertEqual(found.func, create_user)

# class Story10FunctionalTest(TestCase):
# 	def setUp(self):
# 		chrome_options = webdriver.chrome.options.Options()
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
# 		self.selenium.implicitly_wait(25)
# 		super(Story10FunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story10FunctionalTest, self).tearDown()

# 	def test_input_subscribe_form(self):
# 		selenium = self.selenium
# 		# Opening the link
# 		# selenium.get('http://revan-story6.herokuapp.com/landingpage/')
# 		selenium.get('http://127.0.0.1:8000/registrasi/')
# 		# Find the form element
# 		nama = selenium.find_element_by_id('nama')
# 		email = selenium.find_element_by_id('email')
# 		password = selenium.find_element_by_id('pwd')

# 		# Fill the form with data
# 		nama.send_keys('Namaku')
# 		email.send_keys('emailku@gmail.com')
# 		password.send_keys('passwordku')
# 		time.sleep(3)

# 		# submitting the form
# 		submit.send_keys(Keys.RETURN)