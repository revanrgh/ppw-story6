// var acc = document.getElementsByClassName("accordion");
// var i;

// for (i = 0; i < acc.length; i++) {
//     acc[i].addEventListener("click", function() {
//     this.classList.toggle("active");
//     var panel = this.nextElementSibling;
//     if (panel.style.maxHeight){
//       panel.style.maxHeight = null;
//     } else {
//       panel.style.maxHeight = panel.scrollHeight + "px";
//     } 
//   });
// }

// ACCORDION 
$( "#accordion" ).accordion();

// CHANGE THEME
$(document).ready(function(){
    $("button").click(function(){   
        $("h1, h2, h4, h5").toggleClass("text-dark");
        $("body, div.accordion, button").toggleClass("bg-dark");
    });
});

// LOADING PAGE SPINNER
$(window).load(function(){
     $('.loader').delay("slow").hide();
});