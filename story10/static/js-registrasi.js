$(function() {

$('#form-registrasi').on('submit', function(e){
	e.preventDefault();
	str_nama: $('#nama').val();

	$('#button-submit').prop('disabled', true);
	// Mengecek jika input nama masih kosong
	if ($('#nama', this).val() == ''){
		alert("Nama tidak boleh kosong ya!");
	} // Mengecek jika input email masih kosong
	else if ($('#email', this).val() == ''){
		alert("Email tidak boleh kosong ya!");
	} // Mengecek jika input nama masih kosong
	else if ($('#pwd', this).val() == ''){
		alert("Password tidak boleh kosong ya!");
	} else { 
		// Jika semua input form telah diisi
		console.log("form submitted!")
		$('#button-submit').prop('disabled', false);
		$.ajax({
			type: 'POST',
			url: 'create-user/',
			data: {
				nama: $('#nama').val(),
				email: $('#email').val(),
				pwd: $('#pwd').val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			},
			success: function(response){
				console.log($('input[name=csrfmiddlewaretoken]').val())
				alert("Data berhasil disimpan!");
				var html = "";
				for (var i = 0; i < response.users.length; i++) {
					var id = response.users[i].id
					html += "<tr><td>" + response.users[i].nama + "</td>" + "<td><button id='" + id +"' onclick='unsubscribe(" + id  +")'>Unsubscribe</button></td>"
				} 
				$('#subscriber').html(html);
			}
		})
		// Mengosongkan input field setelah form diisi
		$('#nama').val('');
		$('#email').val('');
		$('#pwd').val('');
	}
});

});
$(document).on('keyup', '#email', function(e){
	e.preventDefault();
	// Jika semua input form telah diisi
	console.log("email validated!")
	$.ajax({
		url: 'validate/',
		data: {
			email: $('#email').val(),
			csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
		},
		success: function(response) {
			if (response.is_taken) {
				console.log('Data sudah ada')
				alert("Email yang anda masukkan sudah pernah digunakan sebelumnya")
				$('#email').val(' ');
			}
		}
	})
});

$(document).ready(function(e){
	console.log("display subscriber")
	$.ajax({
		url: 'display/',
		success: function(response){
			var html = "";
			for (var i = 0; i < response.users.length; i++) {
				var id = response.users[i].id
				html += "<tr><td>" + response.users[i].nama + "</td>" + "<td><button id='" + id +"' onclick='unsubscribe(" + id  +")'>Unsubscribe</button></td>"
			} 
			$('#subscriber').html(html);
		}
	})
});

function unsubscribe(id) {
	var pass = prompt("Masukkan Password email Anda")
	$.ajax({
		type: 'POST',
		url: 'delete/',
		data: {
			id: id,
			csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			pass: pass,
		},
		success: function(response){
			var html = "";
			for (var i = 0; i < response.users.length; i++) {
				var id = response.users[i].id;
				html += "<tr><td>" + response.users[i].nama + "</td>" + "<td><button id='" + id +"' onclick='unsubscribe(" + id  +")'>Unsubscribe</button></td>"
			} 
			$('#subscriber').html(html);		
		}
	})
}

function showSubs() {
    var tableSubs = document.getElementById("list-subscriber");

    if (tableSubs.style.display === "none") {
        tableSubs.style.display = "block";
    } else {
        tableSubs.style.display = "none";
    }
}