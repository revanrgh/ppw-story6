from django.test import TestCase, Client
from django.urls import resolve
from .views import index_daftarbuku, login, logout

# Create your tests here.
class Story9UnitTest(TestCase):
	def test_story_6_url_is_exist(self):
		response = Client().get('/daftar-buku/')
		self.assertEqual(response.status_code, 200)

	def test_story_9_using_index_template(self):
		response = Client().get('/daftar-buku/')
		self.assertTemplateUsed(response, 'index-daftarbuku.html')

	def test_story_9_using_index_func(self):
		found = resolve('/daftar-buku/')
		self.assertEqual(found.func, index_daftarbuku)

	def test_story_9_using_login_func(self):
		found = resolve('/daftar-buku/login')
		self.assertEqual(found.func, login)

	# def test_story_9_using_logout_func(self):
	# 	found = resolve('/daftar-buku/logout')
	# 	self.assertEqual(found.func, logout)


	# def json_response_when_added(self):
	# 	response = Client().get('/daftar-buku/get_booklists')
	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertJSONEqual(
	# 		str(response.content, encoding='utf8'),
	# 		{"kind": "books#volumes"}
	# 	)
