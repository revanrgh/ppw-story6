from django.shortcuts import render
from django.http import HttpResponseRedirect        
from .models import Status
from .forms import StatusForm

# Create your views here.
response = {}
def index(request):
    status_res = Status.objects.all()
    response['status_res'] = status_res
    html = 'index.html'
    response['status_form'] = StatusForm
    return render(request, html, response)

def post(request):
    status_form = StatusForm(request.POST or None)
    if(request.method == 'POST' and status_form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        return HttpResponseRedirect('/landingpage')
    else:
        return HttpResponseRedirect('/landingpage')
