from django.db import models

# Create your models here.
class User(models.Model):
	nama = models.CharField(max_length=50)
	email = models.EmailField()
	password = models.CharField(max_length=100)
