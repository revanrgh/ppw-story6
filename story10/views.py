from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import User

# Create your views here.
response = {}
def registration(request):
	html = 'index-registrasi.html'
	return render(request, html)

def create_user(request):
	if request.method == 'POST':
		print("Succeed create_user")
		nama = request.POST['nama']
		email = request.POST['email']
		pwd = request.POST['pwd']

		user = User(nama = nama, email = email, password = pwd)
		user.save()
		users = list(User.objects.values());
		context = {
			'users': users
		}
	return JsonResponse(context)

def validate_email(request):
	print("validate_email")
	email = request.GET['email']
	data = {
		'is_taken': User.objects.filter(email=email).exists()
	}
	return JsonResponse(data)

def displaySubs(request):
	users = list(User.objects.values());
	context = {
		'users': users
	}
	return JsonResponse(context)

def deleteUser(request):
	id = request.POST['id']
	user = User.objects.filter(id = id)
	if request.POST['pass'] == user[0].password:
		user.delete()
	users = list(User.objects.values());
	context = {
		'users': users
	}
	return JsonResponse(context)
