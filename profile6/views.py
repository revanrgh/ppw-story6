from django.shortcuts import render

# Create your views here.
def profile_func(request):
    return render(request, 'profile.html')