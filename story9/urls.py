from django.urls import include, path
from .views import index_daftarbuku, get_booklists, api_search_book, login, logout_view, updateBooks

urlpatterns = [
	path('get_booklists', get_booklists, name="get_booklists"),
	path('search_book/<str:search>', api_search_book, name="search"),
	path('', index_daftarbuku, name="index_daftarbuku"),
	path('login', login, name='login'),
	path('logout', logout_view, name='logout'),
	path('update-books', updateBooks, name="update")
	# path('auth_login', auth_login, name="auth_login")
]

