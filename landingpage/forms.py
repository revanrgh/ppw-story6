from django import forms
from .models import Status

class StatusForm(forms.Form):

	status_attrs = {
        'type': 'text',
        'class' : 'form-control',
        'placeholder':'Masukan status...',
        'id':'id_status'
    }

	status = forms.CharField(label = '', required = True, max_length = 300, widget=forms.TextInput(attrs = status_attrs))

