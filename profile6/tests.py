from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import profile_func

# Create your tests here.
class Challenge6UnitTest(TestCase):
	def test_chal_6_url_is_exist(self):
		response = Client().get('/profile6/')
		self.assertEqual(response.status_code, 200)

	def test_chal_6_using_index_template(self):
		response = Client().get('/profile6/')
		self.assertTemplateUsed(response, 'profile.html')
		
	def test_chal_6_using_index_func(self):
		found = resolve('/profile6/')
		self.assertEqual(found.func, profile_func)